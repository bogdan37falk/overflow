﻿namespace Aplicatie1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.exitButton = new System.Windows.Forms.Button();
            this.buttonPornit = new System.Windows.Forms.Button();
            this.buttonDefectareS2 = new System.Windows.Forms.Button();
            this.buttonReactivare = new System.Windows.Forms.Button();
            this.buttonDefectareS3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonOprit = new System.Windows.Forms.Button();
            this.Bec1 = new System.Windows.Forms.PictureBox();
            this.Bec2 = new System.Windows.Forms.PictureBox();
            this.Bec3 = new System.Windows.Forms.PictureBox();
            this.verticalProgressBar1 = new Aplicatie1.verticalProgressBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Bec1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bec2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bec3)).BeginInit();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exitButton.Location = new System.Drawing.Point(18, 11);
            this.exitButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(38, 51);
            this.exitButton.TabIndex = 50;
            this.exitButton.Text = "X";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // buttonPornit
            // 
            this.buttonPornit.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonPornit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonPornit.Location = new System.Drawing.Point(1720, 92);
            this.buttonPornit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonPornit.Name = "buttonPornit";
            this.buttonPornit.Size = new System.Drawing.Size(52, 54);
            this.buttonPornit.TabIndex = 3;
            this.buttonPornit.Text = "S1";
            this.buttonPornit.UseVisualStyleBackColor = false;
            this.buttonPornit.Click += new System.EventHandler(this.buttonPornit_Click);
            // 
            // buttonDefectareS2
            // 
            this.buttonDefectareS2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonDefectareS2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonDefectareS2.Location = new System.Drawing.Point(1720, 176);
            this.buttonDefectareS2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDefectareS2.Name = "buttonDefectareS2";
            this.buttonDefectareS2.Size = new System.Drawing.Size(52, 54);
            this.buttonDefectareS2.TabIndex = 4;
            this.buttonDefectareS2.Text = "S2";
            this.buttonDefectareS2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDefectareS2.UseVisualStyleBackColor = false;
            this.buttonDefectareS2.Click += new System.EventHandler(this.buttonDefectareS2_Click);
            // 
            // buttonReactivare
            // 
            this.buttonReactivare.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonReactivare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonReactivare.Location = new System.Drawing.Point(1720, 421);
            this.buttonReactivare.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonReactivare.Name = "buttonReactivare";
            this.buttonReactivare.Size = new System.Drawing.Size(52, 54);
            this.buttonReactivare.TabIndex = 5;
            this.buttonReactivare.Text = "S5";
            this.buttonReactivare.UseVisualStyleBackColor = false;
            this.buttonReactivare.Click += new System.EventHandler(this.buttonReactivare_Click);
            // 
            // buttonDefectareS3
            // 
            this.buttonDefectareS3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonDefectareS3.Location = new System.Drawing.Point(1720, 255);
            this.buttonDefectareS3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDefectareS3.Name = "buttonDefectareS3";
            this.buttonDefectareS3.Size = new System.Drawing.Size(52, 54);
            this.buttonDefectareS3.TabIndex = 6;
            this.buttonDefectareS3.Text = "S3";
            this.buttonDefectareS3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonDefectareS3.UseVisualStyleBackColor = false;
            this.buttonDefectareS3.Click += new System.EventHandler(this.buttonDefectareS3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(1720, 338);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(52, 54);
            this.button2.TabIndex = 7;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // buttonOprit
            // 
            this.buttonOprit.BackColor = System.Drawing.Color.Transparent;
            this.buttonOprit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonOprit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonOprit.FlatAppearance.BorderSize = 0;
            this.buttonOprit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOprit.ForeColor = System.Drawing.Color.Transparent;
            this.buttonOprit.Image = global::Aplicatie1.Properties.Resources.circle;
            this.buttonOprit.Location = new System.Drawing.Point(1720, 13);
            this.buttonOprit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonOprit.Name = "buttonOprit";
            this.buttonOprit.Size = new System.Drawing.Size(52, 54);
            this.buttonOprit.TabIndex = 2;
            this.buttonOprit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOprit.UseVisualStyleBackColor = false;
            this.buttonOprit.Click += new System.EventHandler(this.buttonOprit_Click);
            // 
            // Bec1
            // 
            this.Bec1.BackColor = System.Drawing.Color.Transparent;
            this.Bec1.BackgroundImage = global::Aplicatie1.Properties.Resources.circleSmallRed;
            this.Bec1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bec1.Location = new System.Drawing.Point(1461, 107);
            this.Bec1.Name = "Bec1";
            this.Bec1.Size = new System.Drawing.Size(30, 29);
            this.Bec1.TabIndex = 53;
            this.Bec1.TabStop = false;
            // 
            // Bec2
            // 
            this.Bec2.BackColor = System.Drawing.Color.Transparent;
            this.Bec2.BackgroundImage = global::Aplicatie1.Properties.Resources.circleSmallRed;
            this.Bec2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bec2.Location = new System.Drawing.Point(1461, 188);
            this.Bec2.Name = "Bec2";
            this.Bec2.Size = new System.Drawing.Size(30, 29);
            this.Bec2.TabIndex = 54;
            this.Bec2.TabStop = false;
            // 
            // Bec3
            // 
            this.Bec3.BackColor = System.Drawing.Color.Transparent;
            this.Bec3.BackgroundImage = global::Aplicatie1.Properties.Resources.circleSmallRed;
            this.Bec3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bec3.Location = new System.Drawing.Point(1461, 270);
            this.Bec3.Name = "Bec3";
            this.Bec3.Size = new System.Drawing.Size(30, 29);
            this.Bec3.TabIndex = 55;
            this.Bec3.TabStop = false;
            // 
            // verticalProgressBar1
            // 
            this.verticalProgressBar1.Location = new System.Drawing.Point(489, 331);
            this.verticalProgressBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.verticalProgressBar1.Name = "verticalProgressBar1";
            this.verticalProgressBar1.Size = new System.Drawing.Size(62, 802);
            this.verticalProgressBar1.Step = 5;
            this.verticalProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.verticalProgressBar1.TabIndex = 52;
            this.verticalProgressBar1.Click += new System.EventHandler(this.verticalProgressBar1_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Roboto Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(1626, 88);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox1.Size = new System.Drawing.Size(87, 66);
            this.textBox1.TabIndex = 57;
            this.textBox1.Text = "On Button";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Control;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Roboto Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(1357, 107);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox2.Size = new System.Drawing.Size(98, 29);
            this.textBox2.TabIndex = 58;
            this.textBox2.Text = "Pump 1";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Control;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Roboto Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(1357, 186);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox3.Size = new System.Drawing.Size(98, 29);
            this.textBox3.TabIndex = 59;
            this.textBox3.Text = "Pump 2";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Control;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Font = new System.Drawing.Font("Roboto Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(1357, 270);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox4.Size = new System.Drawing.Size(98, 29);
            this.textBox4.TabIndex = 60;
            this.textBox4.Text = "Pump 3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1800, 1231);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Bec3);
            this.Controls.Add(this.Bec2);
            this.Controls.Add(this.Bec1);
            this.Controls.Add(this.verticalProgressBar1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonDefectareS3);
            this.Controls.Add(this.buttonReactivare);
            this.Controls.Add(this.buttonDefectareS2);
            this.Controls.Add(this.buttonPornit);
            this.Controls.Add(this.buttonOprit);
            this.Controls.Add(this.exitButton);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(1200, 500);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Bec1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bec2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bec3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button buttonPornit;
        private System.Windows.Forms.Button buttonDefectareS2;
        private System.Windows.Forms.Button buttonReactivare;
        private System.Windows.Forms.Button buttonDefectareS3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonOprit;
        private System.Windows.Forms.PictureBox Bec1;
        private System.Windows.Forms.PictureBox Bec2;
        private System.Windows.Forms.PictureBox Bec3;
        private verticalProgressBar verticalProgressBar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
    }
}

