var tableRef = document.getElementById('myTable').getElementsByTagName('tbody')[0];
setInterval(() => {
	fetch('../json/queueData.json')
		.then(function (response) {
			if (response.status !== 200) {
				console.log('!!! Status Code:' + response.status);
				return;
			}
			response.text().then(function (queueData) {

				var json = JSON.parse(queueData);
				var obj = JSON.parse(json);
				console.log(obj[1]);
					
				
				for(i=0;i<10;i++)
				{
					var tr = document.createElement('tr');
					var td1 = document.createElement('td');
					var td2 = document.createElement('td');
					var td3 = document.createElement('td');
					var td4 = document.createElement('td');
					var td5 = document.createElement('td');
					var td6 = document.createElement('td');
					var td7 = document.createElement('td');
					var td8 = document.createElement('td');
					var td9 = document.createElement('td');
					var td0 = document.createElement('td');

					var text1 = document.createTextNode(obj[i].id);
					var text2 = document.createTextNode(obj[i].nivel);
					var text3 = document.createTextNode(obj[i].isProcessOn);
					var text4 = document.createTextNode(obj[i].isValveOn);
					var text5 = document.createTextNode(obj[i].isPumpOneOn);
					var text6 = document.createTextNode(obj[i].isPumpTwoOn);
					var text7 = document.createTextNode(obj[i].isPumpThreeOn);
					var text8 = document.createTextNode(obj[i].isPumpOneBroken);
					var text9 = document.createTextNode(obj[i].isPumpTwoBroken);
					var text0 = document.createTextNode(obj[i].lastPumpOn);

					td1.appendChild(text1)
					td2.appendChild(text2)
					td3.appendChild(text3)
					td4.appendChild(text4)
					td5.appendChild(text5)
					td6.appendChild(text6)
					td7.appendChild(text7)
					td8.appendChild(text8)
					td9.appendChild(text9)
					td0.appendChild(text0)

					tr.appendChild(td1);
					tr.appendChild(td2);
					tr.appendChild(td3);
					tr.appendChild(td4);
					tr.appendChild(td5);
					tr.appendChild(td6);
					tr.appendChild(td7);
					tr.appendChild(td8);
					tr.appendChild(td9);
					tr.appendChild(td0);

					tableRef.appendChild(tr);
				}
				
			});
		}
		)
		.catch(error => console.error(error))



}, 350);