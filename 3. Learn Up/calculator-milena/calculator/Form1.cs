﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void AdunareButton_Click(object sender, EventArgs e)
        {
            RezultatTextBox.Text = (float.Parse(Variabila1TextBox.Text) + float.Parse(Variabila2TextBox.Text)).ToString("0.00");

        }

        private void ScadereButton_Click(object sender, EventArgs e)
        {
            RezultatTextBox.Text = (float.Parse(Variabila1TextBox.Text) - float.Parse(Variabila2TextBox.Text)).ToString("0.00");
        }

        private void InmultireButton_Click(object sender, EventArgs e)
        {
            RezultatTextBox.Text = (float.Parse(Variabila1TextBox.Text) * float.Parse(Variabila2TextBox.Text)).ToString("0.00");
        }

        private void ImpartireButton_Click(object sender, EventArgs e)
        {
            RezultatTextBox.Text = (float.Parse(Variabila1TextBox.Text) / float.Parse(Variabila2TextBox.Text)).ToString("0.00");
        }
    }
}
