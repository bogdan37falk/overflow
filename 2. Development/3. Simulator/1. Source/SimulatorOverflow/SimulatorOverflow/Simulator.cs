﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace SimulatorOverflow
{

    

    public struct ProcessState
    {
        public int nivel;
        public bool isProcessOn;
        public bool isValveOn;
        public bool isPumpOneOn;
        public bool isPumpTwoOn;
        public bool isPumpThreeOn;
        public bool isPumpOneBroken;
        public bool isPumpTwoBroken;
        public int lastPumpOn;
    }

    class Simulator
    {
        const int sensorB1 = 15;
        const int sensorB2 = 60;
        const int sensorB3 = 95;
        public ProcessState simProcess;

        public Simulator()
        {
            Console.WriteLine("Starting...");
            System.Threading.Thread.Sleep(1000);
            simProcess.isProcessOn = true;
            simProcess.isValveOn = true;
            simProcess.isPumpOneOn = false;
            simProcess.isPumpTwoOn = false;
            simProcess.isPumpThreeOn = false;
            simProcess.isPumpOneBroken = false;
            simProcess.isPumpTwoBroken = false;
            simProcess.lastPumpOn = 1;

            

        }

        public void TogglePumpOne()
        {
            simProcess.isPumpOneBroken = !simProcess.isPumpOneBroken;
            //simProcess.isPumpOneOn = !simProcess.isPumpOneOn;
        }

        public void TogglePumpTwo()
        {
            simProcess.isPumpTwoBroken = !simProcess.isPumpTwoBroken;
            //simProcess.isPumpTwoOn = !simProcess.isPumpTwoOn;
        }

        public void TogglePumpThree()
        {
            simProcess.isPumpThreeOn = !simProcess.isPumpThreeOn;
        }

        public void TankControl()
        {
            for(; ; )
            {
                
                //Console.WriteLine("Control sees level at " + simProcess.nivel);
                if(simProcess.lastPumpOn==1)
                {
                    if (simProcess.nivel > sensorB1)
                    {
                        simProcess.isPumpOneOn = true;
                        if (simProcess.nivel > sensorB2)
                        {

                            if (simProcess.nivel > sensorB2)
                            {
                                Thread.Sleep(3000);
                                simProcess.isPumpTwoOn = true;
                                simProcess.lastPumpOn = 2;
                            }
                        }

                        if (simProcess.nivel < sensorB2)
                        {
                            simProcess.isPumpTwoOn = false;
                        }

                    }
                    else
                    {
                        simProcess.isPumpOneOn = false;
                        simProcess.isPumpTwoOn = false;
                        
                    }
                    
                }
                else
                {
                    if (simProcess.nivel > sensorB1)
                    {
                        simProcess.isPumpTwoOn = true;
                        if (simProcess.nivel > sensorB2)
                        {

                            if (simProcess.nivel > sensorB2)
                            {
                                Thread.Sleep(3000);
                                simProcess.isPumpOneOn = true;
                                simProcess.lastPumpOn = 1;
                            }
                        }

                        if (simProcess.nivel < sensorB2)
                        {
                            simProcess.isPumpOneOn = false;
                        }

                    }
                    else
                    {
                        simProcess.isPumpTwoOn = false;
                        simProcess.isPumpOneOn = false;
                        
                    }
                    
                }
                
               

            }
            
        }

        public void TankFill(int waterPressure)
        {
            for(; ; )
            {
                simProcess.nivel += waterPressure;
                Console.WriteLine("Level after intake " + simProcess.nivel);
                Thread.Sleep(500);
                
            }
            
        }

        public void TankEmptyPumpOne(int waterDrain)
        {
            for(; ; )
            {
                if(simProcess.isPumpOneOn&&(!simProcess.isPumpOneBroken))
                {
                    
                    simProcess.nivel -= waterDrain;
                    if (simProcess.nivel > 0)
                    {
                        Console.WriteLine("Level after pump one drain " + simProcess.nivel);

                        
                    }
                    else
                    {
                        Console.WriteLine("Tank Empty after pump one drain");
                        simProcess.nivel = 0;
                    }
                    

                }
                Thread.Sleep(500);

            }
        }

        public void TankEmptyPumpTwo(int waterDrain)
        {
            for (; ; )
            {
                if (simProcess.isPumpTwoOn && (!simProcess.isPumpTwoBroken))
                {
                    
                    simProcess.nivel -= waterDrain;
                    if (simProcess.nivel > 0)
                    {
                        Console.WriteLine("Level after pump two drain " + simProcess.nivel);


                    }
                    else
                    {
                        Console.WriteLine("Tank Empty after pump one drain");
                        simProcess.nivel = 0;
                    }
                }
                Thread.Sleep(500);



            }
        }

        public void TankEmptyPumpThree()
        {
            for(; ; )
            {
                if((simProcess.isPumpTwoBroken||simProcess.isPumpOneBroken))
                {
                    simProcess.nivel -= 3;
                    if (simProcess.nivel > 0)
                    {
                        Console.WriteLine("Level after pump three drain " + simProcess.nivel);


                    }
                    else
                    {
                        Console.WriteLine("Tank Empty after pump one drain");
                        simProcess.nivel = 0;
                    }
                }
                Thread.Sleep(500);
            }
        }

        static public string ToReadableByteArray(byte[] bytes)
        {
            return string.Join(", ", bytes);
        }

       // private byte[] _state = new byte[] { 0x00, 0x00 };

       
        public ProcessState GetState()
        {
            return simProcess;
        }
    }
}
