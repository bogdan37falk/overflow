var express = require('express')//
var app2 = express() //
var app = require('http').createServer(handler)
var io = require('socket.io').listen(app)
var fs = require('fs')
var mysql = require('mysql')
var Queue = require('queue-fifo')
var queue = new Queue()
var queue1 = new Queue()
var mySocket = 0
var http = require('http');

function handler(req, res) {
  fs.readFile(__dirname + '/index.html', //Load and display outputs to the index.html file
    function (err, data) {
      if (err) {
        res.writeHead(500)
        return res.end('Error loading index.html')
      }
      res.writeHead(200)
      res.end(data)
    })
}

io.sockets.on('connection', function (socket) {
  console.log('Webpage connected') //Confirmation that the socket has connection to the webpage
  mySocket = socket
})


//HTTP server POST 

const dgram = require('dgram');


http.createServer(function (request, response) {

  if (request.method === 'POST') {
    var data = '';
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Request-Method', '*');
    response.setHeader('Access-Control-Allow-Headers', '*');
    request.on('data', function (chunk) {
      data = chunk.toString();
      //UDP client on 11000
      console.log(data);

    });

    request.on('end', function () {
      // parse the data
      const message = Buffer.from(data);
      const client = dgram.createSocket('udp4');
      client.send(message, 11000, 'localhost', (err) => {
        client.close();
      });
      response.writeHead(200, { 'Content-Type': 'application/json' })
      response.end('{"ok":"true"}');
    });
  }
}).listen(80);




//UDP server on 41181

var server = dgram.createSocket('udp4')

server.on('message', function (msg, rinfo) {
  var obj = JSON.parse(msg)
  var json = JSON.stringify(obj)
  //console.log('Broadcasting Message: ' + json)
  //console.log(obj.nivel)
  fs.writeFile('./Resources/json/simData.json', JSON.stringify(json, null, 2), 'utf-8', function (err) {
    if (err) {
      return console.log(err)
    }
  })

  connection.query('SELECT * FROM overflow_schema.simdata ORDER BY id DESC LIMIT 10', function (error, result, fields) {
    if (error) throw error
    data1 = new Object()
    var i=-1
    result.forEach(query => {

      i++

      var row = {

        id: result[i].id,
        nivel: result[i].nivel,
        isProcessOn: result[i].isProcessOn,
        isValveOn: result[i].isValveOn,
        isPumpOneOn: result[i].isPumpOneOn,
        isPumpTwoOn: result[i].isPumpTwoOn,
        isPumpThreeOn: result[i].isPumpThreeOn,
        isPumpOneBroken: result[i].isPumpOneBroken,
        isPumpTwoBroken: result[i].isPumpTwoBroken,
        lastPumpOn: result[i].lastPumpOn
      }
      data1[i] = row
      
    })
    jsonD1 = JSON.stringify(data1);
    fs.writeFile("./Resources/json/queueData.json",JSON.stringify(jsonD1, null, 2), 'utf-8', function (err) {
      if (err) {
        return console.log(err)
      }
    })
  })
  connection.query('INSERT INTO simdata(nivel,isProcessOn,isValveOn,isPumpOneOn,isPumpTwoOn,isPumpThreeOn,isPumpOneBroken,isPumpTwoBroken,lastPumpOn) VALUES(' + obj.nivel + ',' + Number(obj.isProcessOn) + ',' + Number(obj.isValveOn) + ',' + Number(obj.isPumpOneOn) + ',' + Number(obj.isPumpTwoOn) + ',' + Number(obj.isPumpThreeOn) + ',' + Number(obj.isPumpOneBroken) + ',' + Number(obj.isPumpTwoBroken) + ',' + obj.lastPumpOn + ')', function (error) {
    if (error) throw error
  })


  if (mySocket != 0) {
    mySocket.emit('field', '' + msg)
    mySocket.broadcast.emit('field', '' + msg) //Display the message from the terminal to the webpage
  }
})

server.on('listening', function () {
  var address = server.address() //IPAddress of the server
  console.log('UDP server listening to ' + address.address + ':' + address.port)
})
port = 41181
server.bind(port)

//Database

var connection = mysql.createConnection(
  {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'overflow_schema',
    port: '3307'
  }
)

connection.connect()
connection.query('SELECT * FROM overflow_schema.simdata', function (error, result, fields) {
  if (error) throw error
  var b = -1
  data = new Object()
  result.forEach(query => {

    b++

    var row = {

      id: result[b].id,
      nivel: result[b].nivel,
      isProcessOn: result[b].isProcessOn,
      isValveOn: result[b].isValveOn,
      isPumpOneOn: result[b].isPumpOneOn,
      isPumpTwoOn: result[b].isPumpTwoOn,
      isPumpThreeOn: result[b].isPumpThreeOn,
      isPumpOneBroken: result[b].isPumpOneBroken,
      isPumpTwoBroken: result[b].isPumpTwoBroken,
      lastPumpOn: result[b].lastPumpOn
    }
    data[b] = row
  })
})




//Interface
port2 = 9050
app2.listen(port2)

app2.use(express.static(__dirname + '/Resources'))
app2.get('/', function (req, res) {
  res.sendFile(__dirname + '/Resources/www/Home.html')
})

app2.get('/stats', function (req, res) {
  res.sendFile(__dirname + '/Resources/www/Statistics.html')
})
