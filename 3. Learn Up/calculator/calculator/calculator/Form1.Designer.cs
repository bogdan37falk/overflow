﻿namespace calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Operand1TextBox = new System.Windows.Forms.TextBox();
            this.RezultatTextBox = new System.Windows.Forms.TextBox();
            this.Operand2TextBox = new System.Windows.Forms.TextBox();
            this.PlusButton = new System.Windows.Forms.Button();
            this.MinusButton = new System.Windows.Forms.Button();
            this.InmultireButton = new System.Windows.Forms.Button();
            this.ImpartireButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Operand1TextBox
            // 
            this.Operand1TextBox.Location = new System.Drawing.Point(92, 57);
            this.Operand1TextBox.Name = "Operand1TextBox";
            this.Operand1TextBox.Size = new System.Drawing.Size(100, 20);
            this.Operand1TextBox.TabIndex = 0;
            // 
            // RezultatTextBox
            // 
            this.RezultatTextBox.Location = new System.Drawing.Point(92, 150);
            this.RezultatTextBox.Name = "RezultatTextBox";
            this.RezultatTextBox.Size = new System.Drawing.Size(100, 20);
            this.RezultatTextBox.TabIndex = 1;
            // 
            // Operand2TextBox
            // 
            this.Operand2TextBox.Location = new System.Drawing.Point(92, 99);
            this.Operand2TextBox.Name = "Operand2TextBox";
            this.Operand2TextBox.Size = new System.Drawing.Size(100, 20);
            this.Operand2TextBox.TabIndex = 3;
            // 
            // PlusButton
            // 
            this.PlusButton.Location = new System.Drawing.Point(460, 75);
            this.PlusButton.Name = "PlusButton";
            this.PlusButton.Size = new System.Drawing.Size(75, 23);
            this.PlusButton.TabIndex = 4;
            this.PlusButton.Text = "+";
            this.PlusButton.UseVisualStyleBackColor = true;
            this.PlusButton.Click += new System.EventHandler(this.PlusButton_Click);
            // 
            // MinusButton
            // 
            this.MinusButton.Location = new System.Drawing.Point(460, 125);
            this.MinusButton.Name = "MinusButton";
            this.MinusButton.Size = new System.Drawing.Size(75, 23);
            this.MinusButton.TabIndex = 5;
            this.MinusButton.Text = "-";
            this.MinusButton.UseVisualStyleBackColor = true;
            this.MinusButton.Click += new System.EventHandler(this.MinusButton_Click);
            // 
            // InmultireButton
            // 
            this.InmultireButton.Location = new System.Drawing.Point(460, 169);
            this.InmultireButton.Name = "InmultireButton";
            this.InmultireButton.Size = new System.Drawing.Size(75, 23);
            this.InmultireButton.TabIndex = 6;
            this.InmultireButton.Text = "X";
            this.InmultireButton.UseVisualStyleBackColor = true;
            this.InmultireButton.Click += new System.EventHandler(this.InmultireButton_Click);
            // 
            // ImpartireButton
            // 
            this.ImpartireButton.Location = new System.Drawing.Point(460, 214);
            this.ImpartireButton.Name = "ImpartireButton";
            this.ImpartireButton.Size = new System.Drawing.Size(75, 23);
            this.ImpartireButton.TabIndex = 7;
            this.ImpartireButton.Text = "/";
            this.ImpartireButton.UseVisualStyleBackColor = true;
            this.ImpartireButton.Click += new System.EventHandler(this.ImpartireButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Operand 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Operand 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Rezultat";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ImpartireButton);
            this.Controls.Add(this.InmultireButton);
            this.Controls.Add(this.MinusButton);
            this.Controls.Add(this.PlusButton);
            this.Controls.Add(this.Operand2TextBox);
            this.Controls.Add(this.RezultatTextBox);
            this.Controls.Add(this.Operand1TextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Operand1TextBox;
        private System.Windows.Forms.TextBox RezultatTextBox;
        private System.Windows.Forms.TextBox Operand2TextBox;
        private System.Windows.Forms.Button PlusButton;
        private System.Windows.Forms.Button MinusButton;
        private System.Windows.Forms.Button InmultireButton;
        private System.Windows.Forms.Button ImpartireButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

