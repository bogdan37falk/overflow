﻿namespace calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AdunareButton = new System.Windows.Forms.Button();
            this.ScadereButton = new System.Windows.Forms.Button();
            this.InmultireButton = new System.Windows.Forms.Button();
            this.ImpartireButton = new System.Windows.Forms.Button();
            this.Variabila1TextBox = new System.Windows.Forms.TextBox();
            this.Variabila2TextBox = new System.Windows.Forms.TextBox();
            this.RezultatTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AdunareButton
            // 
            this.AdunareButton.Location = new System.Drawing.Point(272, 58);
            this.AdunareButton.Name = "AdunareButton";
            this.AdunareButton.Size = new System.Drawing.Size(75, 23);
            this.AdunareButton.TabIndex = 0;
            this.AdunareButton.Text = "Adunare";
            this.AdunareButton.UseVisualStyleBackColor = true;
            this.AdunareButton.Click += new System.EventHandler(this.AdunareButton_Click);
            // 
            // ScadereButton
            // 
            this.ScadereButton.Location = new System.Drawing.Point(272, 102);
            this.ScadereButton.Name = "ScadereButton";
            this.ScadereButton.Size = new System.Drawing.Size(75, 22);
            this.ScadereButton.TabIndex = 1;
            this.ScadereButton.Text = "Scadere";
            this.ScadereButton.UseVisualStyleBackColor = true;
            this.ScadereButton.Click += new System.EventHandler(this.ScadereButton_Click);
            // 
            // InmultireButton
            // 
            this.InmultireButton.Location = new System.Drawing.Point(272, 144);
            this.InmultireButton.Name = "InmultireButton";
            this.InmultireButton.Size = new System.Drawing.Size(75, 23);
            this.InmultireButton.TabIndex = 2;
            this.InmultireButton.Text = "Inmultire";
            this.InmultireButton.UseVisualStyleBackColor = true;
            this.InmultireButton.Click += new System.EventHandler(this.InmultireButton_Click);
            // 
            // ImpartireButton
            // 
            this.ImpartireButton.Location = new System.Drawing.Point(272, 184);
            this.ImpartireButton.Name = "ImpartireButton";
            this.ImpartireButton.Size = new System.Drawing.Size(75, 23);
            this.ImpartireButton.TabIndex = 3;
            this.ImpartireButton.Text = "Impartire";
            this.ImpartireButton.UseVisualStyleBackColor = true;
            this.ImpartireButton.Click += new System.EventHandler(this.ImpartireButton_Click);
            // 
            // Variabila1TextBox
            // 
            this.Variabila1TextBox.Location = new System.Drawing.Point(94, 58);
            this.Variabila1TextBox.Name = "Variabila1TextBox";
            this.Variabila1TextBox.Size = new System.Drawing.Size(100, 20);
            this.Variabila1TextBox.TabIndex = 4;
            // 
            // Variabila2TextBox
            // 
            this.Variabila2TextBox.Location = new System.Drawing.Point(94, 102);
            this.Variabila2TextBox.Name = "Variabila2TextBox";
            this.Variabila2TextBox.Size = new System.Drawing.Size(100, 20);
            this.Variabila2TextBox.TabIndex = 5;
            // 
            // RezultatTextBox
            // 
            this.RezultatTextBox.Location = new System.Drawing.Point(94, 144);
            this.RezultatTextBox.Name = "RezultatTextBox";
            this.RezultatTextBox.Size = new System.Drawing.Size(100, 20);
            this.RezultatTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Primul operand";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Al doilea operand";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Rezultat";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 297);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RezultatTextBox);
            this.Controls.Add(this.Variabila2TextBox);
            this.Controls.Add(this.Variabila1TextBox);
            this.Controls.Add(this.ImpartireButton);
            this.Controls.Add(this.InmultireButton);
            this.Controls.Add(this.ScadereButton);
            this.Controls.Add(this.AdunareButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AdunareButton;
        private System.Windows.Forms.Button ScadereButton;
        private System.Windows.Forms.Button InmultireButton;
        private System.Windows.Forms.Button ImpartireButton;
        private System.Windows.Forms.TextBox Variabila1TextBox;
        private System.Windows.Forms.TextBox Variabila2TextBox;
        private System.Windows.Forms.TextBox RezultatTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

