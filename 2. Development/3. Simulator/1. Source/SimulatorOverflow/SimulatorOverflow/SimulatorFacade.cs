﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.ComponentModel;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using System.Net;
using Socket = System.Net.Sockets.Socket;
using Newtonsoft.Json.Linq;

namespace SimulatorOverflow
{

    class SimulatorFacade
    {
     

     
        static Simulator sim;
        static void Main(string[] args)
        {
          
            sim = new Simulator();
        
            Thread simControl = new Thread(() => sim.TankControl());
            simControl.Start();

            Thread simIntake = new Thread(() => sim.TankFill(6));
            simIntake.Start();

            Thread simValveOneOuttake = new Thread(() => sim.TankEmptyPumpOne(3));
            simValveOneOuttake.Start();

            Thread simValveTwoOuttake = new Thread(() => sim.TankEmptyPumpTwo(4));
            simValveTwoOuttake.Start();

            Thread simValveThreeOuttake = new Thread(() => sim.TankEmptyPumpThree());
            simValveThreeOuttake.Start();

            var jsonData = JsonConvert.SerializeObject(sim.simProcess);



            Thread dataFromInterface = new Thread(() => StartClient());
            dataFromInterface.Start();
            while (true)
            {
                SendData("127.0.0.1", 41181, jsonData); //Send data to that host address, on that port, with this 'data' to be sent
                jsonData = JsonConvert.SerializeObject(sim.simProcess);                                     //Note the 41181 port is the same as the one we used in server.bind() in the Javascript file.

                System.Threading.Thread.Sleep(350);
            }
            
        }

      
        public static void StopProcess()
        {
            Environment.Exit(0);
        }

        //private void socketIoManager()
        //{
        //    var socket = IO.Socket("http://127.0.0.1:8888");
        //    socket.On(Quobject.SocketIoClientDotNet.Client.Socket.EVENT_CONNECT, () =>
        //    {
        //        Console.WriteLine("Connected");
        //    });
        //    socket.On("temp", (data) =>
        //     {
        //         var temperature = new { temperature = "" };
        //         var tempValue = JsonConvert.DeserializeAnonymousType((string)data, temperature);
        //         Console.WriteLine((string)tempValue.temperature);
        //     });
        //}

        public static void StartClient()
        {
            while (true)
            {
                byte[] data = new byte[1024];
                IPEndPoint ServerEndPoint = new IPEndPoint(IPAddress.Any, 11000);
                Socket WinSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                WinSocket.Bind(ServerEndPoint);

                Console.Write("Waiting for client");
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                EndPoint Remote = (EndPoint)(sender);
                int recv = WinSocket.ReceiveFrom(data, ref Remote);
                Console.WriteLine("Message received from {0}:", Remote.ToString());
                Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));
                try
                {
                    JObject json = JObject.Parse(Encoding.ASCII.GetString(data, 0, recv));
                    int pumpToStop = Int32.Parse((string)json.SelectToken("stopPump"));
                    Console.WriteLine("Breaking pump " + pumpToStop);
                    Console.WriteLine("");
                    Console.WriteLine("Starting emergency pump 3 ");
                    if(pumpToStop==1)
                    {

                        sim.TogglePumpOne();
                      
                    }
                    if(pumpToStop==2)
                    {
                        sim.TogglePumpTwo();
                    }
                    sim.TogglePumpThree();

                }
                catch (Exception e)
                {

                    throw e;
                }
               
                WinSocket.Close();
            }
           
        }



        public static void SendData(string host, int destPort, string data)
        {
            IPAddress dest = Dns.GetHostAddresses(host)[0]; //Get the destination IP Address
            IPEndPoint ePoint = new IPEndPoint(dest, destPort);
            byte[] outBuffer = Encoding.ASCII.GetBytes(data); //Convert the data to a byte array
            System.Net.Sockets.Socket mySocket = new System.Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp); //Create a socket using the same protocols as in the Javascript file (Dgram and Udp)

            mySocket.SendTo(outBuffer, ePoint); //Send the data to the socket

            mySocket.Close(); //Socket use over, time to close it
        }
    }
}
