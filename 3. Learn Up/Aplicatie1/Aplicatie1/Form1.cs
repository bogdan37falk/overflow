﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebSocketSharp;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Runtime.InteropServices;
using SimulatorOverflow;

namespace Aplicatie1
{
    
    public partial class Form1 : Form
    {
        NetworkStream stream;
        BackgroundWorker bck;
        TcpClient client;


        public struct ProcessState
        {
            public int nivel;
            public bool isProcessOn;
            public bool isValveOn;
            public bool isPumpOneOn;
            public bool isPumpTwoOn;
            public bool isPumpThreeOn;
            public bool isPumpOneBroken;
            public bool isPumpTwoBroken;
            public int lastPumpOn;
        }

        public ProcessState simProcess;

        public Form1()
        {
            InitializeComponent();
            bck = new BackgroundWorker();
            bck.WorkerReportsProgress = true;
            bck.ProgressChanged += Bck_ProgressChanged;
            bck.DoWork += Bck_DoWork;
            bck.RunWorkerAsync();
        }


        private void Bck_DoWork(object sender, DoWorkEventArgs e)
        {
            TcpListener server = null;
            try
            {
                Int32 port = 3000;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(localAddr, port);

                //Start listening for client requests.
                server.Start();

                //Buffer for reading data
                Byte[] bytes = new Byte[36];

                //Enter the listening loop.
                while (true)
                {
                    bck.ReportProgress(0, "Waiting for a connection... ");

                    //Accept TcpClient
                    TcpClient client = server.AcceptTcpClient();
                    bck.ReportProgress(0, "Connected!");


                    stream = client.GetStream();

                    int i;

                    //Get all data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        //display received data by reporting progress to the background worker
                        simProcess = fromBytes(bytes);
                       

                        bck.ReportProgress(0, bytes[1].ToString() + ", " + bytes[6].ToString());
                    }
                
                    
                    //Shutdown and end connection
                    client.Close();
                }

            }
            catch (Exception ex)
            {
                bck.ReportProgress(0, string.Format("SocketException: {0}", ex.ToString()));
            }
        }

        private void Bck_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string data = (string)e.UserState;

            

        }


        public ProcessState fromBytes(byte[] arr)
        {
            ProcessState str = new ProcessState();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (ProcessState)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }

        Thread simInterfaceUpdate;
        private void Form1_Load(object sender, EventArgs e)
        {
          
            
            //should be 32bpp (alpha channel enabled)


        }

        public void SimInterfaceUpdateFunction()
        {
            while (true)
            {

                MethodInvoker vPB = delegate () 
                {
                    
                    verticalProgressBar1.Value = simProcess.nivel;
                  
                 
                
                };

                MethodInvoker b1 = delegate ()
                {
                    if (simProcess.isPumpOneOn)
                    {
                        Bec1.BackgroundImage = Properties.Resources.circleSmallGreen;
                    }
                    else
                    {
                        Bec1.BackgroundImage = Properties.Resources.circleSmallRed;
                    }
                };

                MethodInvoker b2 = delegate ()
                {
                    if (simProcess.isPumpTwoOn)
                    {
                        Bec2.BackgroundImage = Properties.Resources.circleSmallGreen;
                    }
                    else
                    {
                        Bec2.BackgroundImage = Properties.Resources.circleSmallRed;
                    }
                };

                this.Invoke(vPB);
                this.Invoke(b1);
                this.Invoke(b2);
            }

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            simInterfaceUpdate.Abort();
           
            Application.Exit();

        }

        private void buttonOprit_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonPornit_Click(object sender, EventArgs e)
        {
            simInterfaceUpdate = new Thread(() => SimInterfaceUpdateFunction());
            simInterfaceUpdate.Start();
        }

        private void buttonDefectareS2_Click(object sender, EventArgs e)
        {
     
        }

        private void buttonDefectareS3_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void verticalProgressBar1_Click(object sender, EventArgs e)
        {
          
        }

        private void buttonReactivare_Click(object sender, EventArgs e)
        {

            
        }
    }
}

