
function startInterface()
{
console.log("Starting Interface");
setInterval(() => {
    fetch('../json/simData.json')
.then(function(response)
	{
		if(response.status !== 200)
		{
			console.log('!!! Status Code:'+response.status);
			return;
		}
		response.json().then(function(simData)
		{	
          
            var obj=JSON.parse(simData);
      
            var fill=obj.nivel*6.45;
         
			document.getElementById("bar").style.height = fill.toFixed(2)+"px";
			
			console.log(obj.isPumpOneBroken);

			if(obj.isPumpOneBroken)
			document.getElementById("lightValveOne").src="../lightOff.png";
			else
			document.getElementById("lightValveOne").src="lightOn.png";		

			if(obj.isPumpTwoBroken)
			document.getElementById("lightValveTwo").src="../lightOff.png";
			else
			document.getElementById("lightValveTwo").src="lightOn.png";		

			if(obj.isPumpThreeOn)
			document.getElementById("lightValveThree").src="../lightOn.png";
			else
			document.getElementById("lightValveThree").src="lightOff.png";	
		});
	}
)
    .catch(error => console.error(error))
	
	function isEmpty(obj) {
		return Object.keys(obj).length === 0;
    }
	
}, 350);
}