﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace SimulatorOverflow
{
    partial class SimulatorFacade
    {
        public class Sender
        {
            BackgroundWorker _senderWorker;
            ProcessState _process;

            public byte[] getBytes(ProcessState str)
            {
                int size = Marshal.SizeOf(str);
                byte[] arr = new byte[size];

                IntPtr ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(str, ptr, true);
                Marshal.Copy(ptr, arr, 0, size);
                Marshal.FreeHGlobal(ptr);
                return arr;
            }

            public Sender(BackgroundWorker senderWorker, ProcessState Process)
            {
                _senderWorker = senderWorker;
                _process = sim.simProcess;
            }

            public void Send()
            {
                try
                {
                    //Se creaza un TCP client cu adresa de IP si portul 
                    TcpClient client = new TcpClient("127.0.0.1", 3000);

                    while (true)
                    {
                        NetworkStream nwStream = client.GetStream();
                        byte[] bytesToSend = new byte[System.Runtime.InteropServices.Marshal.SizeOf(sim.simProcess)];
                        bytesToSend = getBytes(sim.simProcess);

                        //se apeleaza metoda report progress pentru a face update pe UI (adica in aplicatia consola).
                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                        Thread.Sleep(100);
                    }
                }
                catch (Exception ex)
                {
                    _senderWorker.ReportProgress(0, ex.ToString());
                }

            }
        }

    }
}
